# go-mvc-boilerplate

Simple starter project to help kickstart your MVC-based Go API. Little more than a directory structure to help you start developing core funcionality faster by avoiding the quagmire of generating a directory structure everyone agrees on (no one ever will, so relegate it to a tool as you do with linting). Aside from a directory structure, this project brings some loosely-held opinions:

- [Gin](https://gin-gonic.com/docs/introduction) is used as the HTTP framework
- Business logic is organized into sub-packages (stay organized as project grows)
- Util package separates common tasks (DRY)
- Error "framework" ensures consistently formatted responses

Gin is a lightweight, high-performance HTTP framework ideal for micro-microservices. However, you may have other preferences! To ensure you can easily swap it out for any framework of your choice, it is contained within the controller layer. Regardless of the framework you chose, this is a best practice to ensure future changes of opinon or new requirements are easy to accomodate.

Why MVC? It is an established pattern providing:

- Separation of concerns
- Loose coupling (if you don't abuse individual components)
- Easier re-use (especially of views)
- Simplified parallel development (models can be mocked)
- Higher cohesion (encourage grouping related code into logical components)

# Example Service

The simple API example supports GET and POST methods to exercise each of the M/V/C layers.

```bash
❯ http -v :8080/ping
GET /ping HTTP/1.1
Accept: */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Host: localhost:8080
User-Agent: HTTPie/1.0.3

HTTP/1.1 200 OK
Content-Length: 78
Content-Type: application/json; charset=utf-8
Date: Tue, 14 Jan 2020 01:13:13 GMT

{
    "ping_response": "pong",
    "ping_sequence": 0,
    "ping_time": "2020-01-14T01:13:13Z"
}


❯ http -v POST :8080/ping ping_response=foobah
POST /ping HTTP/1.1
Accept: application/json, */*
Accept-Encoding: gzip, deflate
Connection: keep-alive
Content-Length: 27
Content-Type: application/json
Host: localhost:8080
User-Agent: HTTPie/1.0.3

{
    "ping_response": "foobah"
}

HTTP/1.1 201 Created
Content-Length: 80
Content-Type: application/json; charset=utf-8
Date: Tue, 14 Jan 2020 01:13:19 GMT

{
    "ping_response": "foobah",
    "ping_sequence": 0,
    "ping_time": "2020-01-14T01:13:19Z"
}
```

# TODO

- Flesh out database integration wih more examples
- Extend error "framework" to include more common errors
- Form some opinions around unit and integration testing
- Think about `.gitignore` and other sensibile defaults

# Notes

This is a new project, designed to meet a need for something similar to create-react-app for Go APIs. At this point it is still immature and changing often. As I use it with more projects, the rate of change should slow and included opinions grow in usefuleness. Feel free to suggest improvements!

- [Related blog post](https://blog.devopsdreams.io/everything-old-is-new-again)
- [See also](https://github.com/golang-standards/project-layout)
