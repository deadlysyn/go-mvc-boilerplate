// Package ping is a simple example of controller logic. This is
// where you handle incoming user requets, sanitization and data
// transformation before passing to business logic in services.
package ping

import (
	"net/http"

	"gitlab.com/deadlysyn/go-mvc-boilerplate/utils/dates"

	"github.com/gin-gonic/gin"
	"gitlab.com/deadlysyn/go-mvc-boilerplate/domain/ping"
	"gitlab.com/deadlysyn/go-mvc-boilerplate/services"
	"gitlab.com/deadlysyn/go-mvc-boilerplate/utils/errors"
)

// DefaultResponse handles GET /ping
// Simple example with static content.
func DefaultResponse(c *gin.Context) {
	c.JSON(http.StatusOK, ping.Message{
		Response:  "pong",
		Timestamp: dates.GetNowString(),
	})
}

// CustomResponse handles POST /ping/:message
// You will have many functions with error handling logic to
// ensure valid requests are received before handing off to
// service implementations.
func CustomResponse(c *gin.Context) {
	var msg ping.Message

	if err := c.ShouldBindJSON(&msg); err != nil {
		restErr := errors.NewBadRequestError("invalid json body")
		c.JSON(restErr.Status, restErr)
		return
	}

	result, createErr := services.CreateResponse(msg.Response)
	if createErr != nil {
		c.JSON(createErr.Status, createErr)
		return
	}

	c.JSON(http.StatusCreated, result)
}
