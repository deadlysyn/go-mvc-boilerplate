// Package dates provides helper function to make working
// with timestamps in our API faster and more consistent.
package dates

import "time"

const (
	apiDateFormat = "2006-01-02T15:04:05Z"
)

// GetNow returns the current time in UTC as a date object
func GetNow() time.Time {
	return time.Now().UTC()
}

// GetNowString returns current time in UTC as a string
func GetNowString() string {
	return GetNow().Format(apiDateFormat)
}
