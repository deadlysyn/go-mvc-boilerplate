package app

import (
	"gitlab.com/deadlysyn/go-mvc-boilerplate/controllers/ping"
)

func mapUrls() {
	router.GET("/ping", ping.DefaultResponse)
	router.POST("/ping", ping.CustomResponse)
	// Extend as needed...
}
