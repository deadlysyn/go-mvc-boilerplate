package app

// Lightweight, high-performance HTTP framework used only in controllers
import (
	"os"

	"github.com/gin-gonic/gin"
)

// NOTE: Private variable that is only available in app package
// because this should be the only point in your API where we interact
// with the HTTP framework. This avoids tightly coupling your entire
// service to the framework of the day.
var (
	router = gin.Default()
)

// StartApplication simply maps URLs to supporting functions and
// fires up the listener.
func StartApplication() {
	mapUrls()

	addr := os.Getenv("ADDRESS")
	port := os.Getenv("PORT")

	switch {
	case addr != "" && port != "":
		router.Run(addr + ":" + port)
	case addr == "" && port != "":
		router.Run(":" + port)
	default:
		router.Run(":8080")
	}
}
