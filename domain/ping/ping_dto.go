// Package ping is where we model the actual data structures
// managed by our ping micro-service Methods for validation
// and manipulation also live here.
package ping

import (
	"strings"

	"gitlab.com/deadlysyn/go-mvc-boilerplate/utils/errors"
)

// Message represents the data model or schema of our simple ping API.
type Message struct {
	Response  string `json:"ping_response"`
	Sequence  int    `json:"ping_sequence"`
	Timestamp string `json:"ping_time"`
}

// Validate sanity checks message fields.
func (ping *Message) Validate() *errors.RestErr {
	ping.Response = strings.TrimSpace(ping.Response)
	if ping.Response == "" {
		return errors.NewBadRequestError("invalid response message")
	}
	return nil
}
