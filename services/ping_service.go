// Package services contains functions which implement core business
// logic. Users do not interact directly with these (they talk to
// controllers), and we can keep error handling clean since data
// should be sanitized by our controllers.
package services

import (
	"gitlab.com/deadlysyn/go-mvc-boilerplate/domain/ping"
	"gitlab.com/deadlysyn/go-mvc-boilerplate/utils/dates"
	"gitlab.com/deadlysyn/go-mvc-boilerplate/utils/errors"
)

// CreateResponse generates a new ping response with the specified
// custom message.
func CreateResponse(r string) (*ping.Message, *errors.RestErr) {
	response := &ping.Message{
		Response:  r,
		Sequence:  0,
		Timestamp: dates.GetNowString(),
	}
	if err := response.Validate(); err != nil {
		return nil, err
	}
	return response, nil
}

// Extend as needed...
