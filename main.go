package main

// Update to your cloned project location...
import (
	"gitlab.com/deadlysyn/go-mvc-boilerplate/app"
)

func main() {
	app.StartApplication()
}
